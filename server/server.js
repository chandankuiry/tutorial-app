var express = require('express');

var app = express();
app.use('/server-status', function(req, res) {
    res.send("Server is running")
});
app.listen(4000);
console.log('Server is running at port 4000. To check status browser http://localhost:4000/server-status');